using Gtk 4.0;

template $TransactionDetails: Box {
  orientation: vertical;

  $AdwHeaderBar {
    centering-policy: "strict";

    [title]
    $AdwWindowTitle _details_window_title {
      title: _("Details");
    }

    [start]
    Button back_button {
      valign: center;
      tooltip-text: _("Back");
      icon-name: "go-previous-symbolic";
      clicked => $_on_back_button_clicked();
    }
  }

  ScrolledWindow {
    child: Viewport {
      scroll-to-focus: true;

      $AdwClamp {
        margin-top: "12";
        margin-bottom: "12";
        margin-start: "12";
        margin-end: "12";
        vexpand: "True";
        maximum-size: "1024";
        tightening-threshold: "300";

        Box {
          margin-bottom: 36;
          orientation: vertical;
          spacing: 12;

          Overlay {
            halign: center;

            $AdwAvatar _avatar {
              valign: "center";
              show-initials: "True";
              size: "128";
            }

            [overlay]
            $AdwBin {
              styles [
                "cutout-button",
              ]

              halign: "end";
              valign: "end";

              Button _avatar_edit {
                icon-name: "document-edit-symbolic";

                styles [
                  "circular",
                ]
              }
            }
          }

          Label _name_label {
            label: _("Avatar");
            selectable: true;
            wrap: true;
            wrap-mode: word_char;
            justify: center;

            styles [
              "title",
              "title-1",
            ]
          }

          $AdwPreferencesGroup {
            $AdwActionRow {
              title: _("Amount");

              Label _amount_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("Booking date");

              Label _booking_date_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("Reference");

              Label _reference_label {
                valign: center;
                wrap: true;
              }
            }

            $AdwActionRow {
              title: _("Transaction type");

              Label _transaction_type_label {
                valign: center;
              }
            }
          }

          $AdwPreferencesGroup {
            $AdwActionRow {
              title: _("Value");

              Label _value_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("IBAN");

              Label _iban_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("BIC");

              Label _bic_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("Creditor ID");

              Label _creditor_id_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("Mandate reference");

              Label _mandate_reference_label {
                valign: center;
              }
            }

            $AdwActionRow {
              title: _("End-to-end reference");

              Label _end_to_end_reference_label {
                valign: center;
              }
            }
          }
        }
      }
    };
  }
}
