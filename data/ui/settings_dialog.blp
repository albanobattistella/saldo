using Gtk 4.0;

template $SettingsDialog: $AdwPreferencesWindow {
  can-navigate-back: "True";
  modal: "True";

  $AdwPreferencesPage {
    icon-name: "applications-system-symbolic";
    title: _("General");

    $AdwPreferencesGroup _general_group {
      title: _("General");
      visible: "False";

      $AdwActionRow _dark_theme_row {
        title: _("_Dark Theme");
        subtitle: _("Use dark GTK theme.");
        activatable-widget: "_theme_switch";
        use-underline: "True";

        Switch _theme_switch {
          valign: center;
          action-name: "settings.dark-theme";
        }
      }

      $AdwActionRow {
        /* Translators: Quick Unlock means it only can unlock the safe if the safe was unlocked once. */
        title: _("Enable Quick Unlock");

        /* Translators: Quick Unlock means it only can unlock the safe if the safe was unlocked once. */
        subtitle: _("With Quick Unlock enabled, a safe can be re-entered using the last four characters of the password. If the first try is wrong, the full password is needed to unlock the safe.");
        activatable-widget: "_quickunlock_switch";
        use-underline: "True";

        Switch _quickunlock_switch {
          valign: center;
          action-name: "settings.quickunlock";
        }
      }

      $AdwActionRow {
        /* Translators: Quick Unlock means it only can unlock the safe if the safe was unlocked once. */
        title: _("Enable Fingerprint Unlock");

        /* Translators: Quick Unlock means it only can unlock the safe if the safe was unlocked once. */
        subtitle: _("Use the fingerprint reader to quickly re-enter a locked safe.");
        activatable-widget: "_fingerprint_quickunlock_switch";
        use-underline: "True";

        Switch _fingerprint_quickunlock_switch {
          valign: center;
          action-name: "settings.fingerprint-quickunlock";
        }
      }
    }

    $AdwPreferencesGroup {
      title: _("Safe");

      $AdwActionRow _safe_lock_timeout {
        title: _("_Lock Timeout");
        subtitle: _("Lock safe after n seconds.");
        selectable: "False";
        use-underline: "True";

        SpinButton _lock_timer_spin_button {
          valign: center;
          numeric: true;

          adjustment: Adjustment {
            lower: 1;
            upper: 360;
            step-increment: 1;
          };
        }
      }
    }

    $AdwPreferencesGroup {
      title: _("Banking");

      $AdwActionRow _safe_days {
        title: _("_Sales days");
        subtitle: _("Number of sales days to load.");
        selectable: "False";
        use-underline: "True";

        SpinButton _safe_days_spin_button {
          valign: center;
          numeric: true;

          adjustment: Adjustment {
            lower: 1;
            upper: 100000;
            step-increment: 1;
          };
        }
      }

      $AdwActionRow _automatic_refresh_row {
        title: _("_Automatic Refresh");
        subtitle: _("Update bank transactions every 15 minutes.");
        activatable-widget: "_automatic_refresh_switch";
        use-underline: "True";

        Switch _automatic_refresh_switch {
          valign: center;
          action-name: "settings.automatic-refresh";
        }
      }
    }
  }
}
