# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gettext import gettext as _
from gi.repository import Adw, Gtk
from saldo.backend.backend import BankingBackend


def _listbox_placeholder():
    status_page = Adw.StatusPage()
    status_page.set_icon_name("edit-find-symbolic")
    status_page.set_title(_("No Categories Found"))
    return status_page


@Gtk.Template(resource_path="/org/tabos/saldo/ui/category_dialog.ui")
class CategoryDialog(Adw.AlertDialog):
    __gtype_name__ = "CategoryDialog"

    _listbox = Gtk.Template.Child()
    _search_entry = Gtk.Template.Child()

    def __init__(self, backend: BankingBackend):
        super().__init__()

        self._backend = backend
        self._listbox.set_sort_func(self._sort)
        self._listbox.set_filter_func(self._filter)
        self._listbox.set_placeholder(_listbox_placeholder())

        self._selected: str | None = None

        group = None

        for category in self._backend.CATEGORY_DB:
            row = Adw.ActionRow()
            row.set_title(self._backend.CATEGORY_DB[category]["name"])

            avatar = Adw.Avatar()
            avatar.set_size(32)
            avatar.set_text("saldo")
            avatar.set_icon_name(self._backend.CATEGORY_DB[category]["icon"])
            avatar.set_show_initials(False)
            row.add_suffix(avatar)

            check_box = Gtk.CheckButton()
            check_box.set_valign(Gtk.Align.CENTER)
            check_box.connect("toggled", self._on_radio_button_toggled)
            row.set_activatable_widget(check_box)

            check_box.set_group(group)
            if group is None:
                group = check_box

            row.add_prefix(check_box)

            self._listbox.append(row)

        self._listbox.invalidate_sort()

    def _sort(self, row1: Adw.ActionRow, row2: Adw.ActionRow) -> int:
        title1 = row1.get_title()
        title2 = row2.get_title()

        if title1 < title2:
            return -1
        if title1 > title2:
            return 1
        return 0

    def _filter(self, row: Adw.ActionRow) -> bool:
        search_text = self._search_entry.get_text().lower()
        ret = search_text in row.get_title().lower()

        return ret

    @Gtk.Template.Callback()
    def _on_search_changed(self, _):
        self._listbox.invalidate_filter()

    def _on_radio_button_toggled(self, button: Gtk.Widget) -> None:
        if button.get_active():
            row = button.get_ancestor(Adw.ActionRow)
            label = row.get_title()

            self._selected = label

    def get_selected_category(self):
        if self._selected is None:
            return ""

        for category in self._backend.CATEGORY_DB:
            if self._backend.CATEGORY_DB[category]["name"] == self._selected:
                return category

        return ""
