from __future__ import annotations

import logging
from typing import Any, Callable, Tuple, cast

from gi.repository import Gio, GLib

VERIFY_STATUS_TUPLE = 2


class FingerprintVerifier:
    """Provides easy access to a fingerprint device."""

    def __init__(
        self,
        on_success: Callable,
        on_retry: Callable,
        on_failure: Callable,
    ) -> None:
        """Constructor.

        Might throw an exception if dbus is unavailable, fprintd is missing or
        no device is available.

        Arguments:
        ---------
            on_success: Function to be called on success.
            on_retry: Function to be called if the fingerprint sensor suggests retrying.
            on_failure: Function to be called if the fingerprint did not match or the
                        hardware refused in the process.

        """
        self.on_success = on_success
        self.on_retry = on_retry
        self.on_failure = on_failure

        self._is_claimed = False
        self._device_proxy = None
        self._signal = None

    def connect(self) -> None:
        """Initializes the connection to the fingerprint device.

        Shouldn't be called manually unless disconnect was called on the object before.

        Might throw an exception if dbus is unavailable, fprindt is missing
        or no device is available.
        """
        if self._is_claimed:
            return

        con = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)

        manager_proxy = Gio.DBusProxy.new_sync(
            con,
            Gio.DBusProxyFlags.NONE,
            None,
            "net.reactivated.Fprint",
            "/net/reactivated/Fprint/Manager",
            "net.reactivated.Fprint.Manager",
            None,
        )
        if manager_proxy is None:
            logging.debug("Fprintd not available.")
            return

        try:
            device_str = manager_proxy.call_sync(
                "GetDefaultDevice",
                None,
                Gio.DBusCallFlags.NO_AUTO_START,
                500,
                None,
            )[0]
        except GLib.GError:
            return

        if device_str == "":
            logging.debug("No fingerprint sensor found!")
            return

        self._device_proxy = Gio.DBusProxy.new_sync(
            con,
            Gio.DBusProxyFlags.NONE,
            None,
            "net.reactivated.Fprint",
            device_str,
            "net.reactivated.Fprint.Device",
            None,
        )
        if self._device_proxy is None:
            logging.debug("Failed to find the default fingerprint device.")
            return

        self._signal = self._device_proxy.connect("g-signal", self._signal_handler)
        logging.debug("Successfully connected to the fingerprint device.")

    def disconnect(self) -> None:
        """Removes the handlers from the signal.

        Should be called when the class is not needed anymore.
        """
        if self._is_claimed or self._device_proxy is None:
            return

        self._is_claimed = False
        self._device_proxy.disconnect(self._signal)
        self._signal = None
        self._device_proxy = None
        logging.debug("Disconnected from the fingerprint device.")

    def verify_start(self) -> bool:
        """Starts the fingerprint verification process.

        Returns: True if the fingerprint verification started.

        """
        if self._device_proxy is None:
            return False

        try:
            self._claim_device()
        except GLib.Error:
            logging.exception("Failed to claim device")
            return False

        self._is_claimed = True
        try:
            self._verify_start()
        except GLib.Error as err:
            logging.info(
                "Failed to start fingerprint verification on the device: %s",
                err,
            )
            return False

        logging.debug("Claimed fingerprint device, verification ongoing...")
        return True

    def verify_stop(self) -> None:
        """Stops the fingerprint verification process."""
        if not self._is_claimed:
            return

        self._is_claimed = False

        try:
            self._verify_stop()
            self._release_device()
            logging.debug("Stopped verification, released fingerprint device.")
        except GLib.Error as err:
            logging.debug("Exception while releasing fingerprint device: %s", err)

    #
    # Internal methods
    #

    def _claim_device(self) -> None:
        if self._device_proxy:
            self._device_proxy.call_sync(
                "Claim",
                GLib.Variant("(s)", ("",)),
                Gio.DBusCallFlags.NO_AUTO_START,
                500,
                None,
            )

    def _release_device(self) -> None:
        if self._device_proxy:
            self._device_proxy.call_sync(
                "Release",
                None,
                Gio.DBusCallFlags.NO_AUTO_START,
                500,
                None,
            )

    def _verify_start(self) -> None:
        if self._device_proxy:
            self._device_proxy.call_sync(
                "VerifyStart",
                GLib.Variant("(s)", ("any",)),
                Gio.DBusCallFlags.NO_AUTO_START,
                500,
                None,
            )

    def _verify_stop(self) -> None:
        if self._device_proxy:
            self._device_proxy.call_sync(
                "VerifyStop",
                None,
                Gio.DBusCallFlags.NO_AUTO_START,
                500,
                None,
            )

    retry_results = [
        "verify-retry-scan",
        "verify-swipe-too-short",
        "verify-finger-not-centered",
        "verify-remove-and-retry",
    ]

    def _signal_handler(
        self,
        _proxy: Gio.DBusProxy,
        _sender: str,  # pylint: disable=unused-argument
        signal: str,
        args: tuple[Any],
    ) -> None:
        if signal != "VerifyStatus" or len(args) != VERIFY_STATUS_TUPLE:
            return

        result, done = cast(Tuple[str, bool], args)  # see mypy/issues/1178

        if result in self.retry_results and not done:
            self.on_retry()

        if done:
            if result != "verify-disconnected":
                self.verify_stop()

            if result == "verify-match":
                self.on_success()
            else:
                self.on_failure()
